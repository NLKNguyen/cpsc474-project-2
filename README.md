CPSC474-Project-2

To start up a container to login to interactive shell:
```
./start.sh
```

Equivalent: 
```
docker run --rm -it -v $(pwd)/src:/project nlknguyen/alpine-mpich
```


While inside the container, to compile the program:
```
make
```

To run the program with some preset number of processes and sample input file:
```
make run-1
```
or 
```
make run-4
```

or 
```
make run-10
```

In general:
```
mpirun -n [N] ./main [INPUT_FILE]
```
where [N] is the number of processes to spawn and [INPUT_FILE] is matrix input file
