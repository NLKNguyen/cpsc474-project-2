#include <mpi.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <limits.h>
#include <algorithm>
using namespace std;

// value to be considered as non-value
#define NONE INT_MIN

void print_matrix(int **matrix, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
}


void initialize_buffer(int buff[], int len) {
    for (int i = 0; i < len; i++) {
        buff[i] = NONE;
    }
}

void print_candidates(int buff[], int len) {
    printf("%s \t %s \t %s\n", "row", "col", "sum");
    printf("---------------------\n");
    for (int i = 0; i < len; i+=3) {
        if (buff[i] == NONE) {
            continue;
        }
        printf("%d \t %d \t %d\n", buff[i], buff[i + 1], buff[i + 2]);
    }
}

pair<pair<int, int>, int> find_largest_submatrix(const int buff[], int len) {
    pair<int, int> position;
    int max = NONE;
    for (int i = 0; i < len; i += 3) {
        if (buff[i] == NONE) {
            continue;
        }
        if (buff[i + 2] > max) {
            position.first  = buff[i];
            position.second = buff[i + 1];
            max             = buff[i + 2];
        }

    }
    pair<pair<int, int>, int> result(position, max);
    return result;
}


vector<int> find_local_largest_submatrix(const int *buff, int len) {
    const int work_unit_size = 6;
    int max = NONE;
    int i_pos = 0, j_pos = 0;
    for (int i = 0; i < len; i += work_unit_size) {
        if (buff[i] == NONE) {
            continue;
        }
        // calculate the sum of 2x2 matrix
        int j = 2; // offset to first element in the matrix
        int sum = buff[i + j];
        for (j = 3; j < work_unit_size; j++) {
            sum += buff[i + j];
        }

        // if better candidate is found
        if (sum > max) {
            max = sum;
            i_pos = buff[i];
            j_pos = buff[i + 1];
        }
    }
    vector<int> result({i_pos, j_pos, max});
    return result;
}

vector<int> compute_encoded_data_for_transfer(int **matrix, int rows, int cols) {
    vector<int> send_data;
    for (int i = 0; i < rows - 1; i++) {
        for (int j = 0; j < cols - 1; j++) {
            send_data.insert(send_data.end(), {
                i, j,
                matrix[i][j],     matrix[i][j + 1],
                matrix[i + 1][j], matrix[i + 1][j + 1]
            });
        }
    }
    return send_data;
}

pair<vector<int>, vector<int>> compute_divided_work_loads(vector<int> send_data, int num_processes) {
    const int work_unit_size = 6;
    int total_work_units        = send_data.size() / work_unit_size;
    int min_work_units_per_proc = total_work_units / num_processes;
    int remaining_work_units    = total_work_units % num_processes;

    // cout << "min_work_units_per_proc=" << min_work_units_per_proc << endl;
    // cout << "remaining_work_units=" << remaining_work_units << endl;
    cout << ":: Number of 2x2 sub-matrices = " << total_work_units << endl;
    cout << ":: Length of an encoded 2x2 sub-matrix in serial format = " << work_unit_size << endl;
    cout << ":: Length of data buffer to distribute = " << send_data.size() << endl;
    cout << endl;
    cout << "Deviding workload to processes: " << endl;

    int offset = 0;
    vector<int> send_counts, displacements;
    for (int i = 0; i < num_processes; i++) {
        int send_count = min_work_units_per_proc * work_unit_size;
        // send_counts.push_back(send_count);
        if (remaining_work_units > 0) {
            // distribute each remaining work unit to a process (first-come-first-serve)
            send_count += work_unit_size;
            remaining_work_units--;
        }

        if (send_count == 0) {
            break;
        }

        send_counts.push_back(send_count);
        displacements.push_back(offset);

        printf("  send_counts[%d] = %d\tdisplacements[%d] = %d\n", i, send_count, i, offset);

        offset += send_count;
    }
    pair<vector<int>, vector<int>> result(send_counts, displacements);
    return result;
}

void read_matrix(const string &file_name, int **&matrix, int &rows, int &cols) {
    ifstream file(file_name);

    if(!file)
	{
		cerr << "Failed to open " << file_name << endl;
		return;
	}

    file >> rows >> cols;

    matrix = new int*[rows];
    for (int i = 0; i < rows; i++) {
        matrix[i] = new int[cols];
    }

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            file >> matrix[i][j];
            // cout << matrix[i][j] << " ";
        }
        // cout << endl;
    }
}

int main(int argc, char** argv) {
    const int root = 0;

    // Initialize the MPI environment
    MPI_Init(&argc, &argv);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    vector<int> send_data, send_counts, displacements;
    int *recv_data;
    int recv_data_size;

    int **matrix = nullptr;
    int rows = -1, cols = -1;

    int required_num_processes;
    if (world_rank == root) {
        if (argc < 2) {
            cerr << "Input file is required." << endl;
            return 1;
        }

        read_matrix(argv[1], matrix, rows, cols); // all pass by reference

        if (rows < 0 || cols < 0) {
            cerr << "Invalid input. Program terminated." << endl;
            return 2;
        }

        cout << endl;
        cout << "Input matrix: " << rows << "x" << cols << endl;
        print_matrix(matrix, rows, cols);
        cout << endl;


        ///////////////////////////
        // Prepare data for transfer
        send_data = compute_encoded_data_for_transfer(matrix, rows, cols);

        ///////////////////////////
        // Divide workload to workers
        auto workload = compute_divided_work_loads(send_data, world_size);
        send_counts   = workload.first;  // each process has their own workload
        displacements = workload.second; // element offsets from &send_data corresponding to send_counts

        required_num_processes = send_counts.size(); // because each chunk will be sent to a process
        // cout << "recv_data_size=" << recv_data_size << endl;
    }

    // Broadcast the required number of processes to all
    MPI_Bcast(&required_num_processes, 1, MPI_INT, root, MPI_COMM_WORLD);

    // populate a list of rank numbers needed (<= # of available processes)
    vector<int> required_ranks(required_num_processes);
    std::iota(required_ranks.begin(), required_ranks.end(), 0); // generate [0, 1, 2, ..]

    // Get the group of processes in MPI_COMM_WORLD
    MPI_Group world_group;
    MPI_Comm_group(MPI_COMM_WORLD, &world_group);

    // Construct a group containing only ranks needed (could be all) in world_group to serve the tasks
    MPI_Group sub_group;
    MPI_Group_incl(world_group, required_num_processes, required_ranks.data(), &sub_group);

    // Create a new communicator based on the group
    MPI_Comm sub_comm;
    MPI_Comm_create_group(MPI_COMM_WORLD, sub_group, root, &sub_comm);

    if (sub_comm != MPI_COMM_NULL) { // current rank is part of the sub_comm
        int sub_rank, sub_size;
        MPI_Comm_rank(sub_comm, &sub_rank);
        MPI_Comm_size(sub_comm, &sub_size);

        // We still use rank 0 as root in this communication group.
        // Some of the data we need from before is only available in rank 0
        if (sub_rank == root) {
            recv_data_size = *std::max_element(send_counts.begin(), send_counts.end());
            //               ^ get value that the returned iterator points to
        }

        ///////////////////////////
        // Broadcast received buffer size to all in the working group
        MPI_Bcast(&recv_data_size, 1, MPI_INT, root, sub_comm);


        ///////////////////////////
        // Scatter send_data to everyone with workload described by send_counts and displacements

        // location where each process will receive the scattered data
        recv_data = new int[recv_data_size];
        initialize_buffer(recv_data, recv_data_size);


        MPI_Scatterv(send_data.data(), send_counts.data(), displacements.data(), MPI_INT,
                    recv_data, recv_data_size, MPI_INT,
                    root, sub_comm);

        ///////////////////////////
        // Compute largest submatrix in the received buffer
        vector<int> local_max = find_local_largest_submatrix(recv_data, recv_data_size);
        auto candidate = local_max.data(); // get actual array
        int candidate_size = local_max.size(); // get array size


        ///////////////////////////
        // Gather canidates collected from all the nodes to the root
        int *candidates;
        int candidates_buff_size;

        if (sub_rank == root) {
            candidates_buff_size = candidate_size * world_size;
            candidates = new int[candidates_buff_size]; // each process can return at most one candidate
            initialize_buffer(candidates, candidates_buff_size);
        }

        MPI_Gather( candidate,  candidate_size, MPI_INT,
                    candidates, candidate_size, MPI_INT,
                    root, sub_comm);

        ///////////////////////////
        // Compute the best candidate (global max) and report
        if (sub_rank == root) {
            cout << endl;
            cout << "Gathered result of nodes' local maximum: " << endl;
            print_candidates(candidates, candidates_buff_size);
            cout << endl;

            auto largest_submatrix = find_largest_submatrix(candidates, candidates_buff_size);
            auto position = largest_submatrix.first;
            auto largest_sum = largest_submatrix.second;
            cout << "Largest sum is " << largest_sum<< endl
                << "Found at row = " << position.first << ", col = " << position.second << endl
                << matrix[position.first][position.second]     << " " << matrix[position.first][position.second + 1] << endl
                << matrix[position.first + 1][position.second] << " " << matrix[position.first + 1][position.second + 1] << endl;
        }

        ///////////////////////////
        // Clean up
        MPI_Group_free(&world_group);
        MPI_Group_free(&sub_group);
        MPI_Comm_free(&sub_comm);
    }

    // Finalize the MPI environment.
    MPI_Finalize();
}